RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.navigation_static_links = {
    'Department info' => '#',
    'Projects' => '/admin/projects',
    'Staff' => '#',
    'Students' => '#',
    'Publications' => '#',
    'Pages' => '/admin/page',
    'Users' => '#',
    'Variables' => '#'
  }

  config.model Project do
    list do
      field :title
      field :github
    end
  end

  config.model Page do
    list do
      field :name
      field :path
      field :title
      field :html
      field :uri
    end
  end
end
