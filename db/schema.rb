# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160113153632) do

  create_table "pages", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "path",       null: false
    t.string   "title",      null: false
    t.text     "html"
    t.string   "uri",        null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "pages", ["name"], name: "index_pages_on_name"
  add_index "pages", ["uri"], name: "index_pages_on_uri"

  create_table "projects", force: :cascade do |t|
    t.string   "github"
    t.string   "twitter"
    t.string   "keywords"
    t.boolean  "status"
    t.string   "title"
    t.string   "name",        null: false
    t.string   "logo"
    t.date     "start_date"
    t.date     "end_date"
    t.text     "description"
    t.string   "link"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
