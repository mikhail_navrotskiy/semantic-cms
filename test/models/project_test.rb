require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  def setup
    @project = Project.create! name: 'LOD-IFMO', github: 'https://github.com/aksw/loditmo'
  end

  def test_the_truth
    assert true
  end

  def test_correct_name
    assert_equal 'LOD-IFMO', @project.name
  end

  def test_correct_github
    assert_equal 'https://github.com/aksw/loditmo', @project.github
  end
end
